create table rating(
    rating_id bigserial primary key,
    product_id bigint not null,
    value double precision not null
);

create unique index rating_product_id_idx on rating(product_id);