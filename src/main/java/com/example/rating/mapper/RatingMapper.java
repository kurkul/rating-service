package com.example.rating.mapper;

import com.example.rating.entity.Rating;
import com.example.warehouse.shared.dto.RatingDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface RatingMapper {

    RatingDto convert(Rating rating);

    Rating convert(RatingDto ratingDto);

}
