package com.example.rating.controller.rest;

import com.example.rating.service.RatingService;
import com.example.warehouse.shared.dto.RatingDto;
import com.example.warehouse.shared.dto.ResponseDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.tags.Tags;
import lombok.RequiredArgsConstructor;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/products/ratings")
@Tags(@Tag(name = "Product ratings", description = "Managing ratings of products of a warehouse"))
@RequiredArgsConstructor
public class RatingController {

    private final RatingService ratingService;
    private final KafkaTemplate<String, RatingDto> kafkaTemplate;

    private static final String RATINGS_TOPIC = "ratings";

    @GetMapping("/{productId}")
    @Operation(summary = "Send rating of a product to kafka topic")
    public ResponseDto<Void> sendRatingToKafka(@PathVariable long productId) {
        RatingDto ratingDto = ratingService.getRating(productId);
        kafkaTemplate.send(RATINGS_TOPIC, ratingDto);

        return ResponseDto.success(null);
    }

}
