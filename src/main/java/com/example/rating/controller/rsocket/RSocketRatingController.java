package com.example.rating.controller.rsocket;

import com.example.rating.service.RatingService;
import com.example.warehouse.shared.dto.RatingDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.stereotype.Controller;
import reactor.core.publisher.Flux;

import java.time.Duration;

@Controller
@RequiredArgsConstructor
@Slf4j
public class RSocketRatingController {

    private final RatingService ratingService;

    @MessageMapping("rating-stream")
    Flux<RatingDto> sendRating(long productId) {
        return Flux.interval(Duration.ofSeconds(10)).map(i -> ratingService.getRating(productId));
    }

}
