package com.example.rating.service;

import com.example.warehouse.shared.dto.RatingDto;

public interface RatingService {

    RatingDto getRating(long productId);

}
