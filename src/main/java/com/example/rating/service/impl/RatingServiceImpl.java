package com.example.rating.service.impl;

import com.example.rating.mapper.RatingMapper;
import com.example.rating.repository.RatingRepository;
import com.example.rating.service.RatingService;
import com.example.warehouse.shared.dto.RatingDto;
import com.example.warehouse.shared.exception.CommonException;
import com.example.warehouse.shared.exception.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class RatingServiceImpl implements RatingService {

    private final RatingRepository ratingRepository;
    private final RatingMapper ratingMapper;

    @Override
    public RatingDto getRating(long productId) {
        log.debug("getRating: Starting with: productId: {}", productId);

        try {
            RatingDto rating = ratingRepository.findById(productId)
                    .map(ratingMapper::convert)
                    .orElseThrow(() -> new EntityNotFoundException("Can't find rating by productId " + productId));

            log.info("getRating: Finished: {}", productId);
            return rating;
        } catch (EntityNotFoundException e) {
            log.error("getRating: Failed with: {}", productId, e);
            throw e;
        } catch (Exception e) {
            log.error("getRating: Failed with: {}", productId, e);
            throw new CommonException("Failed to get rating by productId");
        }
    }

}
