package com.example.rating.initializer;

import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.testcontainers.containers.KafkaContainer;
import org.testcontainers.utility.DockerImageName;

public class KafkaInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

    @Override
    public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
        KafkaContainer kafka = new KafkaContainer(DockerImageName.parse("confluentinc/cp-kafka:5.4.3"));

        kafka.start();

        TestPropertyValues.of(
                "spring.kafka.producer.bootstrap-servers=" + kafka.getBootstrapServers(),
                "spring.kafka.consumer.bootstrap-servers=" + kafka.getBootstrapServers()
        ).applyTo(configurableApplicationContext.getEnvironment());
    }

}
