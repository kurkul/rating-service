package com.example.rating.listener;

import com.example.warehouse.shared.dto.RatingDto;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;

@Component
public class RatingListener {

    private final BlockingQueue<RatingDto> ratings = new LinkedBlockingDeque<>();;

    @KafkaListener(topics = "ratings")
    public void listenRatings(RatingDto ratingDto) {
        ratings.add(ratingDto);
    }

    public RatingDto pollRating(long timeoutSeconds) throws InterruptedException {
        return ratings.poll(timeoutSeconds, TimeUnit.SECONDS);
    }

}
