package com.example.rating.controller.rest;

import com.example.rating.initializer.KafkaInitializer;
import com.example.rating.initializer.PostgresInitializer;
import com.example.rating.listener.RatingListener;
import com.example.warehouse.shared.dto.RatingDto;
import com.example.warehouse.shared.dto.ResponseDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(initializers = { PostgresInitializer.class, KafkaInitializer.class })
@ActiveProfiles("test")
@DirtiesContext
public class RatingControllerTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private RatingListener ratingListener;

    @Test
    void when_requestRatingByProductId_expectRatingSentByKafkaProducer() throws Exception {
        ResponseEntity<ResponseDto<RatingDto>> response =
                restTemplate.exchange("/products/ratings/1", HttpMethod.GET,
                        null, new ParameterizedTypeReference<>() {});

        HttpStatusCode actualStatus = response.getStatusCode();
        HttpStatus expectedStatus = HttpStatus.OK;
        RatingDto actualRating = ratingListener.pollRating(10);
        RatingDto expectedRating = RatingDto.builder().ratingId(1L).productId(1L).value(4.5).build();

        assertThat(actualStatus).isEqualTo(expectedStatus);
        assertThat(actualRating).isEqualTo(expectedRating);
    }

    @ParameterizedTest
    @MethodSource
    void when_wrongParamsPassed_expect_givenStatusAndErrorMessageReceived(String url,
                                                                          HttpStatus expectedStatus,
                                                                          String expectedError) {
        ResponseEntity<ResponseDto<Object>> response =
                restTemplate.exchange(url, HttpMethod.GET,
                        null, new ParameterizedTypeReference<>() {});

        assertExchangeFailure(response, expectedStatus, expectedError);
    }

    private static Stream<Arguments> when_wrongParamsPassed_expect_givenStatusAndErrorMessageReceived() {
        return Stream.of(
                Arguments.of("/products/ratings/1000",
                        HttpStatus.NOT_FOUND,
                        "Can't find rating by productId 1000"),
                Arguments.of("/products/ratings/string",
                        HttpStatus.BAD_REQUEST,
                        "Value of param 'productId' is invalid")
        );
    }

    protected <T> void assertExchangeFailure(ResponseEntity<ResponseDto<T>> responseEntity,
                                             HttpStatus expectedStatus,
                                             String expectedError) {

        HttpStatusCode actualStatus = responseEntity.getStatusCode();
        ResponseDto<?> actualResponse = responseEntity.getBody();

        assertThat(actualStatus).isEqualTo(expectedStatus);
        assertThat(actualResponse).isNotNull();
        assertThat(actualResponse.getError()).isEqualTo(expectedError);
        assertThat(actualResponse.getData()).isNull();
    }

}
