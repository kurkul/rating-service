package com.example.rating.controller.rsocket;

import com.example.rating.initializer.PostgresInitializer;
import com.example.warehouse.shared.dto.RatingDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.messaging.rsocket.RSocketRequester;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(initializers = { PostgresInitializer.class })
@ActiveProfiles("test")
@DirtiesContext
public class RSocketRatingControllerTest {

    @Autowired
    RSocketRequester.Builder builder;

    @Test
    void test() {
        RSocketRequester requester = builder.tcp("localhost", 7000);

        Flux<RatingDto> result = requester.route("rating-stream")
                .data(1)
                .retrieveFlux(RatingDto.class)
                .take(1);

        StepVerifier.create(result)
                .expectSubscription()
                .expectNext(RatingDto.builder().ratingId(1L).productId(1L).value(4.5).build())
                .expectComplete()
                .verify();
    }

}
